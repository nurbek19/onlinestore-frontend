import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Col, Grid} from "react-bootstrap";
import CategoryMenu from "../../components/CategoryMenu/CategoryMenu";
import ProductListItem from "../../components/ProductListItem/ProductListItem";
import {fetchCategories, fetchCategoryProducts} from "../../store/actions/products";

class CategoryProducts extends Component {
    componentDidMount() {
        this.props.onFetchCategoryProducts(this.props.match.params.id);
        this.props.fetchCategories();
    }

    componentDidUpdate(prevProps) {

        if (prevProps.match.params.id !== this.props.match.params.id) {
            this.props.onFetchCategoryProducts(this.props.match.params.id);
        }
    }

    render() {
        return (
            <Grid>
                <Col md={2}>
                    <CategoryMenu categories={this.props.categories}/>
                </Col>

                <Col md={10}>
                    {this.props.products.map(product => (
                        <ProductListItem
                            key={product._id}
                            id={product._id}
                            title={product.title}
                            price={product.price}
                            image={product.image}
                        />
                    ))}
                </Col>
            </Grid>
        )
    }
}


const mapStateToProps = state => {
    return {
        products: state.products.products,
        categories: state.products.categories
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchCategoryProducts: id => dispatch(fetchCategoryProducts(id)),
        fetchCategories: () => dispatch(fetchCategories())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryProducts);