import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Panel} from "react-bootstrap";
import Image from "react-bootstrap/es/Image";
import config from "../../config";
import {deleteProduct, fetchSingleProduct} from "../../store/actions/products";

class SingleProduct extends Component {
    componentDidMount() {
        this.props.onFetchProduct(this.props.match.params.id);
    }

    render() {
        const product = this.props.product;
        const user = this.props.user;
        let showBtn = false;

        if (user && product) {
            if (user._id === product.user._id) {
                showBtn = true;
            }
        }

        return (
            product ? <Panel>
                <Panel.Body>
                    <Image
                        style={{maxWidth: '100%'}}
                        src={config.apiUrl + '/uploads/' + product.image}
                        thumbnail
                    />

                    <h2>{product.title}</h2>
                    <p>
                        <strong>Category:</strong> {product.category.title}
                    </p>

                    <p><strong>Price:</strong> {product.price} $</p>


                    <p>{product.description}</p>

                    <p><strong>Seller:</strong> {product.user.displayName}</p>

                    <p><strong>Phone number:</strong> {product.user.phoneNumber}</p>

                    {showBtn && <Button
                        bsStyle="primary"
                        onClick={() => this.props.deleteProduct(product._id, user.token)}
                    >Delete product</Button>
                    }
                </Panel.Body>
            </Panel> : <p>Loading...</p>
        )
    }
}


const mapStateToProps = state => {
    return {
        product: state.products.product,
        user: state.users.user
    }
};


const mapDispatchToProps = dispatch => {
    return {
        onFetchProduct: id => dispatch(fetchSingleProduct(id)),
        deleteProduct: (id, token) => dispatch(deleteProduct(id, token))
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(SingleProduct);