import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct, fetchCategories} from "../../store/actions/products";

class NewProduct extends Component {

    componentDidMount() {
        this.props.fetchCategories();
    }

    createProduct = productData => {
        this.props.onProductCreated(productData, this.props.user.token);
    };

    render() {
        return(
            <Fragment>
                <PageHeader>New  product</PageHeader>
                <ProductForm onSubmit={this.createProduct} options={this.props.categories}/>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
  return {
      user: state.users.user,
      categories: state.products.categories
  }
};

const mapDispatchToProps = dispatch => {
  return {
      onProductCreated: (productData, token) => dispatch(createProduct(productData, token)),
      fetchCategories: () => dispatch(fetchCategories())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewProduct);