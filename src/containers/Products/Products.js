import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Col, Grid} from "react-bootstrap";
import {fetchCategories, fetchProducts} from "../../store/actions/products";

import ProductListItem from '../../components/ProductListItem/ProductListItem';
import CategoryMenu from "../../components/CategoryMenu/CategoryMenu";

class Products extends Component {
    componentDidMount() {
        this.props.onFetchProducts();
        this.props.fetchCategories();
    }

    render() {
        return (
                <Grid>
                    <Col md={2}>
                        <CategoryMenu categories={this.props.categories} />
                    </Col>
                    <Col md={10}>
                        <h2>All items</h2>

                        {this.props.products.map(product => (
                            <ProductListItem
                                key={product._id}
                                id={product._id}
                                title={product.title}
                                price={product.price}
                                image={product.image}
                            />
                        ))}
                    </Col>
                </Grid>

        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.products.products,
        categories: state.products.categories
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchProducts: () => dispatch(fetchProducts()),
        fetchCategories: () => dispatch(fetchCategories())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);