import {
    FETCH_CATEGORIES_SUCCESS,
    FETCH_CATEGORY_PRODUCTS_SUCCESS,
    FETCH_PRODUCT_SUCCESS,
    FETCH_SINGLE_PRODUCT_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    products: [],
    product: null,
    categories: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCT_SUCCESS:
            return {...state, products: action.products};
        case FETCH_SINGLE_PRODUCT_SUCCESS:
            return {...state, product: action.product};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.categories};
        case FETCH_CATEGORY_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        default:
            return state;
    }
};

export default reducer;