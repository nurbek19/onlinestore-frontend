import axios from '../../axios-api';
import {
    CREATE_PRODUCT_SUCCESS,
    DELETE_PRODUCT_SUCCESS, FETCH_CATEGORIES_SUCCESS, FETCH_CATEGORY_PRODUCTS_SUCCESS,
    FETCH_PRODUCT_SUCCESS,
    FETCH_SINGLE_PRODUCT_SUCCESS
} from "./actionTypes";
import {push} from "react-router-redux";

export const fetchProductSuccess = products => {
    return {type: FETCH_PRODUCT_SUCCESS, products}
};

export const fetchProducts = () => {
  return dispatch => {
      return axios.get('/products').then(response => {
          dispatch(fetchProductSuccess(response.data));
      })
  }
};

export const fetchCategoryProductsSuccess = products => {
  return {type: FETCH_CATEGORY_PRODUCTS_SUCCESS, products}
};

export const fetchCategoryProducts = id => {
    return dispatch => {
        return axios.get(`/products?category=${id}`).then(response => {
            dispatch(fetchCategoryProductsSuccess(response.data));
        })
    }
};


export const createProductSuccess = () => {
  return {type: CREATE_PRODUCT_SUCCESS}
};

export const createProduct = (productData, token) => {
  return dispatch => {
      const headers = {'Token': token};

      return axios.post('/products', productData, {headers}).then(() => {
          dispatch(createProductSuccess());
          dispatch(push('/'));
      })
  }
};

export const fetchSingleProductSuccess = product => {
  return {type: FETCH_SINGLE_PRODUCT_SUCCESS, product};
};

export const fetchSingleProduct = id => {
    return dispatch => {
        return axios.get(`/products/${id}`).then(response => {
            dispatch(fetchSingleProductSuccess(response.data));
        })
    }
};

export const deleteProductSuccess = () => {
  return {type: DELETE_PRODUCT_SUCCESS};
};

export const deleteProduct = (id, token) => {
  return dispatch => {
      const headers = {'Token': token};

      return axios.delete(`/products/${id}`, {headers}).then(() => {
          dispatch(deleteProductSuccess());
          dispatch(push('/'));
      })
  }
};

export const fetchCategoriesSuccess = categories => {
  return {type: FETCH_CATEGORIES_SUCCESS, categories};
};

export const fetchCategories = () => {
  return dispatch => {
      return axios.get('/categories').then(response => {
          dispatch(fetchCategoriesSuccess(response.data));
      })
  }
};
























