import React, {Component} from 'react';

import {Route, Switch} from "react-router-dom";
import Products from "./containers/Products/Products";
import NewProduct from "./containers/NewProduct/NewProduct";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import SingleProduct from "./containers/SingleProduct/SingleProduct";
import CategoryProducts from "./containers/CategoryProducts/CategoryProducts";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Products}/>
                    <Route path="/products/new" exact component={NewProduct}/>
                    <Route path="/products/:id" exact component={SingleProduct}/>
                    <Route path="/categories/:id" exact component={CategoryProducts}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
