import React, {Fragment} from 'react';
import {Nav, NavItem} from 'react-bootstrap';
import {LinkContainer} from "react-router-bootstrap";

const UserMenu = ({user, logout}) => {
    const navTitle = (
        <Fragment>
            Hello, <b>{user.username}</b>
        </Fragment>
    );

    return (
        <Nav pullRight>
            <NavItem>{navTitle}</NavItem>
            <LinkContainer to="/products/new" exact>
                <NavItem>Add new item</NavItem>
            </LinkContainer>
            <NavItem onClick={logout}>Logout</NavItem>
        </Nav>
    )
};

export default UserMenu;