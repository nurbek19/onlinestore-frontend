import React from 'react';
import {Nav, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const CategoryMenu = props => {
    return (
        <Nav bsStyle="pills" stacked>
            <LinkContainer to="/" exact>
                <NavItem>
                    All items
                </NavItem>
            </LinkContainer>
            {props.categories.map(category => (
                <LinkContainer key={category._id} to={`/categories/${category._id}`} exact>
                    <NavItem>
                        {category.title}
                    </NavItem>
                </LinkContainer>
            ))}
        </Nav>
    )
};

export default CategoryMenu;