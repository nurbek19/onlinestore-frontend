import React, {Component} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import FormElement from "../UI/Form/FormElement";

class ProductForm extends Component {
    state = {
        title: '',
        price: '',
        description: '',
        image: '',
        category: 'null'
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Form horizontal onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Title"
                    placeholder="Enter product title"
                    autoComplete="new-title"
                    type="text"
                    required={true}
                    value={this.state.title}
                    changeHandler={this.inputChangeHandler}
                />

                <FormElement
                    propertyName="price"
                    title="Price"
                    placeholder="Enter product price"
                    autoComplete="new-price"
                    type="number"
                    required={true}
                    value={this.state.price}
                    changeHandler={this.inputChangeHandler}
                />

                <FormElement
                    propertyName="description"
                    title="Description"
                    placeholder="Enter descripton"
                    autoComplete="new-description"
                    type="textarea"
                    required={true}
                    value={this.state.description}
                    changeHandler={this.inputChangeHandler}
                />

                <FormElement
                    propertyName="image"
                    title="Image"
                    placeholder="Add file"
                    autoComplete="new-file"
                    type="file"
                    required={true}
                    changeHandler={this.fileChangeHandler}
                />

                <FormGroup controlId="formControlsSelect">
                    <Col componentClass={ControlLabel} sm={2}>
                        Category
                    </Col>
                    <Col sm={10}>
                        <FormControl
                            componentClass="select" required
                            name="category"
                            value={this.state.category}
                            onChange={this.inputChangeHandler}
                        >
                            <option value="null" disabled="true">Select category</option>
                            {this.props.options.map(category => (
                                <option key={category._id} value={category._id}>{category.title}</option>
                            ))}
                        </FormControl>
                    </Col>
                </FormGroup>

                <FormGroup>
                    <Col smOffset={2} sm={10}>
                        <Button bsStyle="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default ProductForm;
