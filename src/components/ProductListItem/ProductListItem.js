import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';
import config from '../../config';


const ProductListItem = props => {

    return (
        <Panel key={props.id}>
            <Panel.Body>
                <Image
                    style={{width: '150px'}}
                    src={config.apiUrl + '/uploads/' + props.image}
                    thumbnail
                />

                <Link to={"/products/" + props.id} style={{marginLeft: '10px'}}>{props.title}</Link>
                <strong style={{marginLeft: '10px'}}>
                    {props.price} KGS
                </strong>
            </Panel.Body>
        </Panel>
    )
};

ProductListItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string
};

export default ProductListItem;